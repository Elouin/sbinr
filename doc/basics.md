# sbinr - Static BINary manager

Cli tool to manage programs distributed as static linked binaries.

## Overview

Some programs are mainly distributed via static linked binaries, which is fine in its self, but kind of annoying to manage. This tool is there to help with installing, updating and removing them. There will be some sort of repo file, which links to the download urls of the programs. These then will be donwloaded and put into the path, at user level.

## Paths

The default configuration is to have the programs at user level.

### Config

So config will go into `~/.config/sbinr`.

### Binaries

The binaries will propably go to `~/.local/bin` by default, since some distributions seem to have this in the path by default and i couldn't find any information on where to put binaries in the home of a user from xdgs rules.


## Repositories

The repositories are single, propably toml, files containing basic information and the download location of the available programs.

```toml
name = "NameOfTheRepository"
description = "Description of the repository"

[[binaries]]
name = "ProgramName"
description = "Short description of the program"
versions."1.2".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.2".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.2".architectures.freebsd-x64.link = "https://download.link/to/binary"
versions."1.1".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.1".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.1".architectures.freebsd-x64.link = "https://download.link/to/binary"
versions."1.0".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.0".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.0".architectures.freebsd-x64.link = "https://download.link/to/binary"

[[binaries]]
name = "ProgramName"
description = "Short description of the program"
versions."1.2".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.2".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.2".architectures.freebsd-x64.link = "https://download.link/to/binary"
versions."1.1".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.1".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.1".architectures.freebsd-x64.link = "https://download.link/to/binary"
versions."1.0".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.0".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.0".architectures.freebsd-x64.link = "https://download.link/to/binary"

[[binaries]]
name = "ProgramName"
description = "Short description of the program"
versions."1.2".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.2".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.2".architectures.freebsd-x64.link = "https://download.link/to/binary"
versions."1.1".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.1".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.1".architectures.freebsd-x64.link = "https://download.link/to/binary"
versions."1.0".architectures.linux-x64.link = "https://download.link/to/binary"
versions."1.0".architectures.linux-armv7.link = "https://download.link/to/binary"
versions."1.0".architectures.freebsd-x64.link = "https://download.link/to/binary"
```