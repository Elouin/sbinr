use clap::{Parser, Subcommand};

/// Little cli tool to managae programms packed as static binaries
#[derive(Parser)]
#[command(name = "sbinr")]
#[command(version = "0.1.0")]
#[command(about = "Manages programs packed as static binaries", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    // Look for updates
    Fetch,
    // Initialize config/db
    Init,
    // Install a new program
    Install,
    // Update all programs
    Update,
    // Remove a program
    Remove,
    // Search for a program
    Search,
}

fn main() {
    let cli = Cli::parse();

    match &cli.command {
        Commands::Init => {
            println!("init was used")
        },
        _ => todo!()
    }
}